//
// Created by Carson Farmer on 8/21/17.
// Copyright © 2017 Set Inc. All rights reserved.
//
// Some docs, tests, and code ported from https://github.com/twitter/algebird and https://github.com/addthis/stream-lib
// Licensed under the Apache License, Version 2.0 (the "License")
//

import Foundation

/// MARK: Primary Sketch Structure

/// Count-Min Sketch (CMS)
///
/// A Count-Min sketch is a probabilistic data structure used for summarizing streams of data in sub-linear space.
///
/// It works as follows:
///  - Let `(ϵ, δ)` be two parameters that describe the confidence in our error estimates, and let
/// `depth = ceil(ln 1/delta)` `width = ceil(e / eps)`.
/// Then:
///  - Take `depth` pairwise independent hash functions `h_i`, each of which maps onto the domain `[0, w - 1]`.
///  - Create a 2-dimensional table of counts, with `depth` rows and `width` columns, initialized with all zeroes.
///  - When a new element x arrives in the stream, update the table of counts by setting `counts[i, h_i[x]] += 1`,
///    for each `1 <= i <= d`.
///
/// As an example application, suppose you want to estimate the number of times an element `x` has appeared in a data
/// stream so far. The Count-Min sketch estimate of this frequency is min_i { counts[i, h_i[x]] }
/// With probability at least `1 - δ`, this estimate is within `ϵ * N` of the true frequency (i.e., `true
/// frequency <= estimate <= true frequency + ϵ * N`), where N is the total size of the stream so far.
///
/// - References:
///     - Graham Cormode and S. Muthukrishnan. An improved data stream summary: The Count-Min sketch and its
///       applications. 2004. https://doi.org/10.1016/j.jalgor.2003.12.001
///     - http://dimacs.rutgers.edu/%7Egraham/pubs/papers/cm-full.pdf

struct CountMinSketch<Item: CustomHashable> {
    /// Number of hash functions (also: number of rows in counting table). Can be derived from `delta`.
    private(set) var depth: Int
    /// Number of counters per hash function (also: number of columns in counting table). Can be derived from `epsilon`.
    private(set) var width: Int
    /// The total (exact) count of items stored in the sketch.
    private(set) var count: Int
    /// Private array of hash function seeds.
    private(set) var hashes: [Int]
    /// Private 2d hash table.
    private(set) var table: [Int]
    /// One-sided error bound on the error of each point query, i.e. frequency estimate.
    var epsilon: Double { return exp(1) / Double(width) }
    /// A bound on the probability that a query estimate does not lie within some small interval around the 'truth'.
    var delta: Double { return 1 - 1 / exp(Double(depth)) }

    /// Initialize a new Count-Min Sketch.
    ///
    /// - Parameters:
    ///     - epsilon: One-sided error bound on the error of each point query, i.e. frequency estimate.
    ///     - delta: A bound on the probability that a query estimate does not lie within some small interval (an
    ///       interval that depends on `epsilon`) around the truth.
    ///     - seed: A seed to initialize the random number generator used to create the pairwise independent hash
    ///       functions.
    /// - Returns: A fully specified Count-Min Sketch.
    ///
    /// - Notes:
    ///     - `epsilon` and `delta` are parameters that bound the error of each query estimate. For example, errors in
    ///        answering point queries (e.g., how often has element x appeared in the stream described by the sketch?)
    ///        are often of the form: "with probability p >= 1 - delta, the estimate is close to the truth by
    ///        some factor depending on epsilon."
    ///     - A CountMinSketch is a generic structure that takes as input any CustomHashable object. The `Item` type is
    ///       the type of items you want to count. You must provide an explicit `CustomHashable` generic `Item`,  and
    ///       this module ships with CustomHashable, Int, Float, Double, Bool, and String. If you wish to count a
    ///       custom type that is not supported out of the box, you simply need to ensure your custom type conforms to
    ///       the CustomHashable protocol. Basically it must provide an integer hash value via a `hashValue` public var.
    ///       CustomHashable has the same requirements as: https://developer.apple.com/documentation/swift/hashable.
    init(epsilon: Double = 0.0001, delta: Double = 0.9999, seed: Int? = nil) {
        precondition(0 < delta && delta < 1, "Delta (δ) must lie between 0 and 1.")
        let width = Int(ceil(exp(1) / epsilon))
        let depth = Int(ceil(-log(1 - delta)))
        self.init(depth: depth, width: width, count: 0, seed: seed)
    }

    /// Initialize a new Count-Min Sketch.
    ///
    /// - Parameters:
    ///     - depth: Number of hash functions (also: number of rows in counting table).
    ///     - width: Number of counters per hash function (also: number of columns in counting table).
    ///     - count: The total (exact) count of items stored in the sketch. Defaults to 0.
    ///     - seed: A seed to initialize the random number generator used to create the pairwise independent hash
    ///       functions.
    /// - Returns: A fully specified Count-Min Sketch.
    init(depth: Int, width: Int, count: Int = 0, seed: Int? = nil) {
        precondition(depth < 100, "depth must be smaller as it causes precision errors when computing delta (δ).")
        self.depth = depth
        self.width = width
        self.count = count
        self.table = [Int](repeating: 0, count: depth * width)
        if let seed = seed { srand48(seed) }
        self.hashes = (0..<depth).map { _ in Int(round(drand48() * Double(Int.max))) }
    }

    /// Internal initializer with specified hash table and hash seeds.
    internal init(table: [Int], hashes: [Int], count: Int = 0) {
        precondition(table.count % hashes.count == 0, "hashes length must be multiple of table length.")
        depth = hashes.count
        width = table.count / depth
        self.count = count
        self.table = table
        self.hashes = hashes
    }

    private func hash(_ item: Item, index: Int) -> Int {
        // Typically, we would use 'depth' pair-wise independent hash functions of the form
        //
        //   h_i(x) = a_i * x + b_i (mod p)
        //
        // But for this particular application, setting b_i does not matter (since all it does is shift the results of
        // a particular hash), so we omit it (by setting b_i to 0) and simply use hash functions of the form
        //
        //   h_i(x) = a_i * x (mod p)
        //
        // where a_i are independent random uniform integer values initialized in the init method.
        let hashed = item.hashValue &* hashes[index] // Watch out for overflows!
        // An apparently super fast way of computing x mod 2^p-1
        // See http://www.cs.princeton.edu/courses/archive/fall09/cos521/Handouts/universalclasses.pdf
        // page 149, right after Proposition 7.
        return (hashed >> 32 & Int.max) % width
    }

    /// Insert CustomHashable item into the sketch.
    ///
    /// - Parameters:
    ///     - item: Any CustomHashable item.
    ///     - count: The count associated with the new `item`. Defaults to 1.
    public mutating func update(with item: Item, count: Int = 1) {
        // For negative increments we'd need to use the median instead of minimum, and accuracy would suffer somewhat.
        precondition(count >= 0, "Negative increments not allowed.")
        (0..<depth).forEach { table[$0 * width + hash(item, index: $0)] += count }
        self.count += count
    }

    /// Estimate the count for a given CustomHashable item.
    ///
    /// - Parameter: item Any CustomHashable item for which to estimate a count
    /// - Returns: Estimated `item` count in the sketch
    ///
    /// - Notes: The estimate is correct within ϵ * count, with probability δ.
    public func query(for item: Item) -> Int? {
        let counts = (0..<depth).map { table[$0 * width + hash(item, index: $0)] }
        return counts.reduce(Int.max, Swift.min)
    }

    /// Merges count min sketches to produce a count min sketch for their combined streams
    ///
    /// - Parameter: estimators A variadic list of CountMinSketches to merge
    /// - Returns: A single (optional) merged estimator
    public static func merge(_ sketches: CountMinSketch...) -> CountMinSketch? {
        return merge(sketches)
    }

    /// Merges count min sketches to produce a count min sketch for their combined streams
    ///
    /// - Parameter: estimators An array of CountMinSketches to merge
    /// - Returns: A single (optional) merged estimator
    public static func merge(_ sketches: [CountMinSketch]) -> CountMinSketch? {
        guard let first = sketches.first else { return nil }
        var table = first.table
        var count = first.count
        sketches.dropFirst().forEach { estimator in
            precondition(first =~ estimator, "Cannot merge estimators with different parameters.")
            for i in 0..<table.count {
                table[i] += estimator.table[i]
            }
            count += estimator.count
        }
        return CountMinSketch(table: table, hashes: first.hashes, count: count)
    }

    /// MARK: Syntactic Sugar for Queries

    /// Estimate the count for a given CustomHashable item.
    subscript(item: Item) -> Int? {
        return query(for: item)
    }

    /// Estimate the counts for a variadic list of CustomHashable items.
    subscript(items: Item...) -> [Int?] {
        return items.map { query(for: $0) }
    }
}
