//
// Created by Carson Farmer on 8/21/17.
// Copyright © 2017 Set Inc. All rights reserved.
// Licensed under the Apache License, Version 2.0 (the "License")
//

#import <UIKit/UIKit.h>

//! Project version number for CountSwiftly.
FOUNDATION_EXPORT double CountSwiftlyVersionNumber;

//! Project version string for CountSwiftly.
FOUNDATION_EXPORT const unsigned char CountSwiftlyVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CountSwiftly/PublicHeader.h>


