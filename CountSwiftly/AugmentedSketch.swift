//
// Created by Carson Farmer on 8/25/17.
// Copyright © 2017 Set Inc. All rights reserved.
// Licensed under the Apache License, Version 2.0 (the "License")
//

import Foundation

/// MARK: Primary Sketch Structure

struct AugmentedSketch<Item: CustomHashable> {
    /// Swift implementation of the Augmented sketch (ASketch):
    /// Pratanu Roy, A. Khan, and G. Alonso. Augmented Sketch: Faster and More Accurate Stream Processing. 2016.
    /// https://doi.org/10.1145/2882903.2882948

    /// Stores high-frequency items and their associated counts. newCount denotes the estimated (over-estimation)
    /// frequency of an item. The difference between newCount and oldCount represents the (exact) aggregated frequency
    /// that has accumulated. Note that in the paper, they explore the use of a Vector and Heap variant of this hash
    /// table. This has the benefit that they can support SIMD instructions, and in practice is potentially faster
    /// because it avoids random access and pointer chasing. For our requirements, a hash table is sufficient, and much
    /// easier to implement!
    var filter: [Item: (newCount: Int, oldCount: Int)] = [:]
    /// Classical Count-Min sketch for probabilistic counting/frequency estimation
    var sketch: CountMinSketch<Item>
    /// Maximum size of filter
    let filterSize: Int
    /// The total (exact) count of items stored in the sketch.
    private(set) var count: Int

    /// Initialize a new Augmented Sketch (ASketch).
    ///
    /// - Parameters:
    ///     - epsilon: One-sided error bound on the error of each point query, i.e. frequency estimate.
    ///     - delta: A bound on the probability that a query estimate does not lie within some small interval (an
    ///       interval that depends on `epsilon`) around the truth.
    ///     - filterSize: The size (number of keys) of dictionary used to store high-frequency items. Defaults to 32.
    ///     - count: The total (exact) count of items stored in the sketch. Defaults to 0.
    ///     - seed: A seed to initialize the random number generator used to create the pairwise independent hash
    ///       functions.
    /// - Returns: A fully specified Augmented Sketch.
    ///
    /// - Notes:
    ///     - `epsilon` and `delta` are parameters that bound the error of each query estimate. For example, errors in
    ///        answering point queries (e.g., how often has element x appeared in the stream described by the sketch?)
    ///        are often of the form: "with probability p >= 1 - delta, the estimate is close to the truth by
    ///        some factor depending on epsilon." These parameters are passed directly to the underlying Count-Min
    ///        sketch for tracking less-frequent items.
    ///     - An AugmentedSketch is a generic structure that takes as input any CustomHashable object. See the
    ///       documentation for the CountMinSketch to details.
    init(epsilon: Double = 0.0001, delta: Double = 0.9999, filterSize: Int = 32, count: Int = 0, seed: Int? = nil) {
        sketch = CountMinSketch(epsilon: epsilon, delta: delta, seed: seed)
        self.filterSize = filterSize
        self.count = count
    }

    /// Initialize a new Augmented Sketch (ASketch).
    ///
    /// - Parameters:
    ///     - depth: Number of hash functions (also: number of rows in counting table).
    ///     - width: Number of counters per hash function (also: number of columns in counting table).
    ///     - filterSize: The size (number of keys) of dictionary used to store high-frequency items. Defaults to 32.
    ///     - count: The total (exact) count of items stored in the sketch. Defaults to 0.
    ///     - seed: A seed to initialize the random number generator used to create the pairwise independent hash
    ///       functions.
    /// - Returns: A fully specified Count-Min Sketch.
    init(depth: Int, width: Int, filterSize: Int = 32, count: Int = 0, seed: Int? = nil) {
        sketch = CountMinSketch(depth: depth, width: width, seed: seed)
        self.filterSize = filterSize
        self.count = count
    }

    /// Insert CustomHashable item into the sketch.
    ///
    /// - Parameters:
    ///     - item: Any CustomHashable item.
    ///     - count: The count associated with the new `item`. Defaults to 1.
    public mutating func update(with item: Item, count: Int = 1) {
        // Algorithm 1 - Stream processing algorithm for ASketch p. 1453
        if filter[item] != nil {
            filter[item]?.newCount += count
        } else if filter.count < filterSize {
            filter[item] = (newCount: count, oldCount: 0)
        } else {
            sketch.update(with: item, count: count)
            if let estimate = sketch.query(for: item),
               let (key, minFreq) = filter.min(by: { $0.value.newCount < $1.value.newCount }),
               estimate > minFreq.newCount {
                if minFreq.newCount - minFreq.oldCount > 0 {
                    sketch.update(with: key, count: minFreq.newCount - minFreq.oldCount)
                }
                filter.removeValue(forKey: key)
                filter[item] = (newCount: estimate, oldCount: estimate)
            }
        }
        self.count += count
    }

    /// Estimate the count for a given CustomHashable item.
    ///
    /// - Parameter: item Any CustomHashable item for which to estimate a count
    /// - Returns: Estimated `item` count in the sketch
    ///
    /// - Notes: The estimate is correct within ϵ * count, with probability δ. In practice, ASketch should be
    ///   significantly more accurate than a vanilla Count-Min sketch.
    public func query(for item: Item) -> Int? {
        // Algorithm 2 - Query processing algorithm for ASketch p. 1453
        return filter[item].map { $0.newCount } ?? sketch[item]
    }

    /// Return the top `filterSize` items (heavy-hitters).
    ///
    /// - Returns: Dictionary of the top `filterSize` most frequently occurring items.
    public func filtered() -> [Item: Int] {
        var dict = [Item: Int]()
        for (key, value) in filter {
            dict[key] = value.newCount
        }
        return dict
    }

    /// MARK: Syntactic Sugar for Queries

    /// Estimate the count for a given CustomHashable item.
    subscript(item: Item) -> Int? {
        return query(for: item)
    }

    /// Estimate the counts for a variadic list of CustomHashable items.
    subscript(items: Item...) -> [Int?] {
        return items.map { query(for: $0) }
    }

}