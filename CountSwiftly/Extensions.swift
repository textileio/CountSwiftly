//
// Created by Carson Farmer on 8/22/17.
// Copyright © 2017 Set Inc. All rights reserved.
// Licensed under the Apache License, Version 2.0 (the "License")
//

import Foundation

/// MARK: CustomHashable Protocol

protocol CustomHashable: Hashable {
    // Because the Apple docs say:
    // "Hash values are not guaranteed to be equal across different executions of your program.
    // Do not save hash values to use during a future execution."
    var hashValue: Int { get }
}

extension String: CustomHashable {
    // String is the unpredictable one as far as I can tell. So we implement two standard hashing functions here.
    // As long as a String's unicode scalar values don't change somehow we should be safe here...

    // djb2 and sdbm hash functions as computed properties from https://useyourloaf.com/blog/swift-hashable/
    var djb2Hash: Int {
        return unicodeScalars.map { $0.value }.reduce(5381) { ($0 << 5) &+ $0 &+ Int($1) }
    }
    var sdbmHash: Int {
        return unicodeScalars.map { $0.value }.reduce(0) { Int($1) &+ ($0 << 6) &+ ($0 << 16) - $0 }
    }
    // Default to using djb2 hash function
    var hashValue: Int {
        return djb2Hash
    }
}

extension Int: CustomHashable {
    // Int is pretty straight-forward, so no need to re-implement.
}

extension Double: CustomHashable {
    // Double and Float are also straight-forward, and will only be unpredictable if their bit patterns change.
    // In the simplest cases, this is how these two are implemented in Swift by default.
    var hashValue: Int {
        return Int(bitPattern: UInt(bitPattern))
    }
}

extension Float: CustomHashable {
    var hashValue: Int {
        return Int(bitPattern: UInt(bitPattern))
    }
}

extension Bool: CustomHashable {
    // Bool is obvious, and also shouldn't change. This is actually how it is implemented in Swift by default.
    var hashValue: Int {
        return self ? 1 : 0
    }
}

/// MARK: Sketch is Equatable

precedencegroup Equivalence {
    higherThan: ComparisonPrecedence
    lowerThan: AdditionPrecedence
}

infix operator =~ : Equivalence
infix operator !~ : Equivalence

extension CountMinSketch: Equatable {
    public static func =~ (lhs: CountMinSketch, rhs: CountMinSketch) -> Bool {
        if lhs.depth != rhs.depth { return false }
        if lhs.width != rhs.width { return false }
        if lhs.hashes != rhs.hashes { return false }
        return true
    }

    public static func !~ (lhs: CountMinSketch, rhs: CountMinSketch) -> Bool {
        return !(lhs =~ rhs)
    }

    public static func == (lhs: CountMinSketch, rhs: CountMinSketch) -> Bool {
        if lhs !~ rhs { return false }
        let compare: [Bool] = zip(lhs.table, rhs.table).flatMap { $0.0 == $0.1 }
        return !compare.contains(false)
    }
}

extension AugmentedSketch: Equatable {
    public static func =~ (lhs: AugmentedSketch, rhs: AugmentedSketch) -> Bool {
        if lhs.sketch !~ rhs.sketch { return false }
        if lhs.filterSize != rhs.filterSize { return false }
        return lhs.filter.keys.elementsEqual(rhs.filter.keys) // TODO: Make sure ordering doesn't matter?
    }

    public static func !~ (lhs: AugmentedSketch, rhs: AugmentedSketch) -> Bool {
        return !(lhs =~ rhs)
    }

    public static func == (lhs: AugmentedSketch, rhs: AugmentedSketch) -> Bool {
        if lhs !~ rhs { return false }
        if lhs.sketch != rhs.sketch { return false }
        let lhsSorted = lhs.filter.values.sorted(by: { $0.newCount < $1.newCount }).map({ $0.newCount })
        let rhsSorted = rhs.filter.values.sorted(by: { $0.newCount < $1.newCount }).map({ $0.newCount })
        return lhsSorted == rhsSorted
    }
}

/// MARK: Sketch is CustomStringConvertible

extension CountMinSketch: CustomStringConvertible {
    public var description: String {
        return "CMSketch{depth: \(depth), width: \(width), count: \(count), table: \(table), hashes: \(hashes)}"
    }
}

extension AugmentedSketch: CustomStringConvertible {
    public var description: String {
        return "ASketch{filterSize: \(filterSize), count: \(count), " +
                "filter: \(filter.description), sketch: \(sketch.description)}"
    }
}