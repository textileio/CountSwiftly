//
// Created by Carson Farmer on 8/21/17.
// Copyright © 2017 Set Inc. All rights reserved.
//
// Some docs, tests, and code ported from https://github.com/twitter/algebird and https://github.com/addthis/stream-lib
// Licensed under the Apache License, Version 2.0 (the "License")
//

import XCTest
@testable import CountSwiftly

extension String {

    static func random(length: Int = 10) -> String {
        let base = "abcde"
        var randomString: String = ""

        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.characters.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }
}

class CountMinSketchTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSize() {
        var sketch = CountMinSketch<Int>(epsilon: 0.00001, delta: 0.99999, seed: 1)
        XCTAssertEqual(sketch.count, 0)

        sketch.update(with: 1, count: 11)
        sketch.update(with: 2, count: 22)
        sketch.update(with: 3, count: 33)

        let expectedSize = 11 + 22 + 33
        XCTAssertEqual(sketch.count, expectedSize)
    }

    func testSizeCanStoreInt64() {
        let confidence = 0.999
        let epsilon = 0.0001
        let seed = 1

        var sketch = CountMinSketch<Int>(epsilon: epsilon, delta: confidence, seed: seed)

        let freq1 = Int(Int32.max)
        let freq2 = 156

        sketch.update(with: 1, count: freq1)
        sketch.update(with: 2, count: freq2)

        let newSketch = CountMinSketch.merge(sketch, sketch)
        let expectedSize = 2 &* (freq1 &+ freq2)

        if let testSketch = newSketch {
            XCTAssertEqual(expectedSize, testSketch.count)
        } else {
            XCTFail("Merged sketch returned empty")
        }
    }

    func testAccuracy() { // WARNING: Slow
        let seed = 7364181
        srand48(seed)
        let numItems = 10000
        var xs = [Int](repeating: 0, count: numItems)
        let maxScale: UInt32 = 20
        for i in 0..<numItems {
            let scale = arc4random_uniform(maxScale)
            xs[i] = Int(arc4random_uniform(1 << scale))
        }

        let epsOfTotalCount = 0.0001
        let confidence = 0.99

        var sketch = CountMinSketch<Int>(epsilon: epsOfTotalCount, delta: confidence, seed: seed)
        var actualFreq = [Int](repeating: 0, count: Int(1 << maxScale))
        for x in xs {
            sketch.update(with: x)
            actualFreq[x] += 1
        }

        var numErrors = 0
        for i in 0..<actualFreq.count {
            let ratio = Double(sketch.query(for: i)! - actualFreq[i]) / Double(numItems)
            if ratio > epsOfTotalCount {
                numErrors += 1
            }
        }
        let pCorrect = 1.0 - Double(numErrors) / Double(actualFreq.count)
        XCTAssertTrue(pCorrect > confidence, "Confidence not reached: required \(confidence) reached \(pCorrect)")
    }

    func testMerge() { // WARNING: Slow!
        let numToMerge = 5
        let cardinality = 10000

        let epsOfTotalCount = 0.0001
        let confidence = 0.99
        let seed = 7364181

        let maxScale: UInt32 = 20
        var vals: [Int] = []

        var baseline = CountMinSketch<Int>(epsilon: epsOfTotalCount, delta: confidence, seed: seed)
        var sketches = [CountMinSketch<Int>]()
        for i in 0..<numToMerge {
            sketches.append(CountMinSketch<Int>(epsilon: epsOfTotalCount, delta: confidence, seed: seed))
            for _ in 0..<cardinality {
                let scale = arc4random_uniform(maxScale)
                let val = Int(arc4random_uniform(1 << scale))
                vals.append(val)
                sketches[i].update(with: val)
                baseline.update(with: val)
            }
        }

        let merged = CountMinSketch<Int>.merge(sketches)

        if let merged = merged {
            XCTAssertEqual(baseline.count, merged.count)
            XCTAssertEqualWithAccuracy(baseline.delta, merged.delta, accuracy: baseline.delta / 100)
            XCTAssertEqualWithAccuracy(baseline.epsilon, merged.epsilon, accuracy: baseline.epsilon / 100)
            for val in vals {
                XCTAssertEqual(baseline.query(for: val), merged.query(for: val))
            }
        } else {
            XCTFail("Merged sketch returned empty")
        }
    }

    func testMergeEmpty() {
        XCTAssertNil(CountMinSketch<String>.merge())
        XCTAssertNil(CountMinSketch<Int>.merge())
    }

    func testEquals() {
        let eps1 = 0.0001
        let eps2 = 0.000001
        let confidence = 0.99
        let seed = 1

        var sketch1 = CountMinSketch<Int>(epsilon: eps1, delta: confidence, seed: seed)
        XCTAssertEqual(sketch1, sketch1)

        var sketch2 = CountMinSketch<Int>(epsilon: eps1, delta: confidence, seed: seed)
        XCTAssertEqual(sketch1, sketch2)

        var sketch3 = CountMinSketch<Int>(epsilon: eps1, delta: confidence/2, seed: seed)
        XCTAssertNotEqual(sketch1, sketch3)

        sketch1.update(with: 1, count: 123)
        sketch2.update(with: 1, count: 123)
        XCTAssertEqual(sketch1, sketch2)

        sketch1.update(with: 1, count: 4)
        XCTAssertNotEqual(sketch1, sketch2)

        var sketch4 = CountMinSketch<Int>(epsilon: eps1, delta: confidence, seed: seed)
        let sketch5 = CountMinSketch<Int>(epsilon: eps2, delta: confidence, seed: seed)
        XCTAssertNotEqual(sketch4, sketch5)

        sketch3.update(with: 1, count: 7)
        sketch4.update(with: 1, count: 7)
        XCTAssertNotEqual(sketch4, sketch5)
    }

    func testZeroAtStart() {
        let sketch = CountMinSketch<Int>(depth: 10, width: 5)
        for _ in 1...10 {
            XCTAssertEqual(sketch.query(for: Int(arc4random_uniform(100))), 0)
        }
    }

    func testCountsOverestimate() {
        // Create an array of length-3 random strings
        let strings: [String] = (0..<1000).map { _ in String.random(length: 3) }
        var counts: [String: Int] = [:]

        var sketch = CountMinSketch<String>(depth: 10, width: 5)
        for string in strings {
            counts[string] = (counts[string] ?? 0) + 1
            sketch.update(with: string)
        }

        for string in Set(strings) {
            XCTAssertGreaterThanOrEqual(sketch.query(for: string)!, counts[string]!)
        }
    }

    func testParameters() {
        let eps = 0.0001
        let confidence = 0.99
        let seed = 1

        let sketch = CountMinSketch<Int>(epsilon: eps, delta: confidence, seed: seed)
        XCTAssertEqual(sketch.width, 27_183)
        XCTAssertEqual(sketch.depth, 5)
        XCTAssertEqual(sketch.count, 0)
        XCTAssertEqualWithAccuracy(sketch.epsilon, eps, accuracy: 1e-6)
        XCTAssertEqualWithAccuracy(sketch.delta, confidence, accuracy: 1e-2)
    }

    func testSimpleUsage() {
        let n = 1000
        var sketch = CountMinSketch<String>(depth: 10, width: 5)
        for _ in 0..<n {
            sketch.update(with: "a")
        }
        XCTAssertEqual(sketch.query(for: "a"), n)
        XCTAssertEqual(sketch.query(for: "b"), 0)
        XCTAssertEqual(sketch.count, n)
    }

    func testSyntacticSugar() {
        var sketch = CountMinSketch<String>(depth: 10, width: 5)
        XCTAssertEqual(sketch.query(for: "a"), sketch["a"])
        sketch.update(with: "a")
        XCTAssertEqual(sketch.query(for: "a"), sketch["a"])
    }

    func testAddGreaterThanOne() {
        var sketch = CountMinSketch<String>(depth: 10, width: 5)
        sketch.update(with: "a", count: 123)
        XCTAssertEqual(sketch.query(for: "a"), 123)
    }

    func testDescription() {
        var sketch = CountMinSketch<String>(depth: 5, width: 5, seed: 1234)
        sketch.update(with: "a", count: 123)
        print(sketch)
        let expectedString = "CMSketch{depth: 5, width: 5, count: 123, " +
                "table: [0, 123, 0, 0, 0, 0, 0, 0, 123, 0, 123, 0, 0, 0, 0, 0, 0, 123, 0, 0, 123, 0, 0, 0, 0], " +
                "hashes: [6833383553770749952, 1978734802533875712, 3119305999351119872, 2980406081017675776, " +
                "9211382780712878080]}"
        XCTAssertEqual(sketch.description, expectedString)
    }

    func testDifferentTypes() {
        var integer = CountMinSketch<Int>(depth: 10, width: 5, seed: 1234)
        integer.update(with: 1234)
        XCTAssertEqual(integer.query(for: 1234), 1)

        var string = CountMinSketch<String>(depth: 10, width: 5, seed: 1234)
        string.update(with: "1234")
        XCTAssertEqual(string.query(for: "1234"), 1)

        var float = CountMinSketch<Float>(depth: 10, width: 5, seed: 1234)
        float.update(with: Float(1234.5678))
        XCTAssertEqual(float.query(for: Float(1234.5678)), 1)

        var double = CountMinSketch<Double>(depth: 10, width: 5, seed: 1234)
        double.update(with: 1234.5678)
        XCTAssertEqual(double.query(for: 1234.5678), 1)

        var boolean = CountMinSketch<Bool>(depth: 10, width: 5, seed: 1234)
        boolean.update(with: true)
        XCTAssertEqual(boolean.query(for: true), 1)
    }
}

class AugmentedSketchTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testBasic() {
        var sketch = AugmentedSketch<Int>(epsilon: 0.0001, delta: 0.999, filterSize: 5, seed: 1)
        sketch.update(with: 1, count: 11)
        sketch.update(with: 2, count: 22)
        sketch.update(with: 3, count: 11)
        sketch.update(with: 3, count: 22)

        // Filter should contains all elements when inputs less than filterSize
        XCTAssertEqual(sketch.filter.count, 3)
        // Filter should contain correct keys
        XCTAssertEqual(sketch.filter.keys.map { $0 }.sorted(), [1, 2, 3])
        // Sum of values in filter should be equal to total count
        XCTAssertEqual(sketch.filter.values.reduce(0) { $0 + $1.newCount }, 11 + 22 + 33)
        // Underlying sketch should be empty
        XCTAssertEqual(sketch.sketch.count, 0)
    }

    func testFilterSize() {
        srand48(1234) // Seed random number generator

        // Create an array of length-3 random strings
        let strings: [String] = (0..<1000).map { _ in String.random(length: 3) }

        // Add strings to sketch
        var sketch = AugmentedSketch<String>(epsilon: 0.0001, delta: 0.999, filterSize: 5, seed: 1)
        for string in strings { sketch.update(with: string) }

        // Filter should never grow any larger than filterSize
        XCTAssertEqual(sketch.filter.count, 5)
        XCTAssertEqual(sketch.count, 1000)
    }

    func testFilterCounts() {
        srand48(1234) // Seed random number generator

        // Create an array of length-3 random strings
        let strings: [String] = (0..<1000).map { _ in String.random(length: 3) }

        // Add strings to sketch and keep track of frequencies
        var sketch = AugmentedSketch<String>(epsilon: 0.0001, delta: 0.999, filterSize: 5, seed: 1)
        var counts: [String: Int] = [:]
        for string in strings {
            counts[string] = (counts[string] ?? 0) + 1
            sketch.update(with: string)
        }

        // The smallest count in the filter _should_ be greater than or equal to the max in the table
        let (key, filterMin) = sketch.filter.min { $0.value.newCount < $1.value.newCount }!
        let tableMax = sketch.sketch.query(for: key)
        XCTAssertGreaterThanOrEqual(filterMin.newCount, tableMax!)

        // The top 3 most frequent strings should always be in the _filter_ and their counts should be exact
        let topThree = counts.sorted { $0.value > $1.value }[0..<3]
        for (key, value) in topThree {
            XCTAssertNotNil(sketch.filter[key])
            XCTAssertEqual(sketch.query(for: key)!, value)
        }
    }

    func testEquals() {
        let eps1 = 0.0001
        let eps2 = 0.000001
        let confidence = 0.99
        let seed = 1

        var sketch1 = AugmentedSketch<Int>(epsilon: eps1, delta: confidence, filterSize: 10, seed: seed)
        XCTAssertEqual(sketch1, sketch1)

        var sketch2 = AugmentedSketch<Int>(epsilon: eps1, delta: confidence, filterSize: 10, seed: seed)
        XCTAssertEqual(sketch1, sketch2)

        var sketch3 = AugmentedSketch<Int>(epsilon: eps1, delta: confidence/2, filterSize: 10, seed: seed)
        XCTAssertNotEqual(sketch1, sketch3)

        sketch1.update(with: 1, count: 123)
        sketch2.update(with: 1, count: 123)
        XCTAssertEqual(sketch1, sketch2)

        sketch1.update(with: 1, count: 4)
        XCTAssertNotEqual(sketch1, sketch2)

        var sketch4 = AugmentedSketch<Int>(epsilon: eps1, delta: confidence, filterSize: 10, seed: seed)
        let sketch5 = AugmentedSketch<Int>(epsilon: eps2, delta: confidence, filterSize: 10, seed: seed)
        XCTAssertNotEqual(sketch4, sketch5)

        sketch3.update(with: 1, count: 7)
        sketch4.update(with: 1, count: 7)
        XCTAssertNotEqual(sketch4, sketch5)
    }

    func testAccuracy() { // WARNING: Slow!
        // The augmented sketch should provide more accurate estimates overall
        // TODO: Make sure this test is really working... seems to always produce 100% recall?
        let seed = 7364181
        srand48(seed)
        let numItems = 1000
        var xs = [Int](repeating: 0, count: numItems)
        let maxScale: UInt32 = 20
        for i in 0..<numItems {
            let scale = arc4random_uniform(maxScale)
            xs[i] = Int(arc4random_uniform(1 << scale))
        }

        let epsOfTotalCount = 0.001
        let confidence = 0.99

        var aSketch = AugmentedSketch<Int>(epsilon: epsOfTotalCount, delta: confidence, filterSize: 32, seed: seed)
        var cmSketch = CountMinSketch<Int>(epsilon: epsOfTotalCount, delta: confidence, seed: seed)
        var actualFreq = [Int](repeating: 0, count: Int(1 << maxScale))
        for x in xs {
            cmSketch.update(with: x)
            aSketch.update(with: x)
            actualFreq[x] += 1
        }

        var cmsErrors = 0
        var akErrors = 0
        for i in 0..<actualFreq.count {
            let cmsRatio = Double(cmSketch.query(for: i)! - actualFreq[i]) / Double(numItems)
            if cmsRatio > epsOfTotalCount {
                cmsErrors += 1
            }
            let akRatio = Double(aSketch.query(for: i)! - actualFreq[i]) / Double(numItems)
            if akRatio > epsOfTotalCount {
                akErrors += 1
            }
        }
        let cCorrect = 1.0 - Double(cmsErrors) / Double(actualFreq.count)
        let aCorrect = 1.0 - Double(akErrors) / Double(actualFreq.count)
        XCTAssertGreaterThan(cCorrect, confidence)
        XCTAssertGreaterThan(aCorrect, confidence)
        XCTAssertGreaterThanOrEqual(aCorrect, cCorrect)
    }

    func testSyntacticSugar() {
        var sketch = AugmentedSketch<String>(epsilon: 0.0001, delta: 0.999, filterSize: 5)
        XCTAssertEqual(sketch.query(for: "a"), sketch["a"])
        sketch.update(with: "a")
        XCTAssertEqual(sketch.query(for: "a"), sketch["a"])
    }

    func testDescription() {
        var sketch = AugmentedSketch<String>(epsilon: 0.6, delta: 0.99, filterSize: 5, seed: 1234)
        sketch.update(with: "a", count: 123)
        let expectedString = "ASketch{filterSize: 5, count: 123, filter: [\"a\": (newCount: 123, oldCount: 0)], " +
                "sketch: CMSketch{depth: 5, width: 5, count: 0, " +
                "table: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], " +
                "hashes: [6833383553770749952, 1978734802533875712, 3119305999351119872, 2980406081017675776, " +
                "9211382780712878080]}}"
        XCTAssertEqual(sketch.description, expectedString)
    }

    func testFiltered() {
        var sketch = AugmentedSketch<Int>(epsilon: 0.0001, delta: 0.999, filterSize: 5, seed: 1)
        for i in (1...20) {
            sketch.update(with: i, count: 31 - i)
        }

        // Only the top five should be in the filtered dictionary
        let queried = sketch.filtered()
        for i in (1...5) {
            XCTAssertNotNil(queried[i])
        }
        for i in (6...20) {
            XCTAssertNil(queried[i])
        }
    }
}