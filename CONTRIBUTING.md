# Contributing

Set uses a common Pull / Merge Request flow:

1. Open an issue on Gitlab
2. Start a new local branch
3. Do work
4. Push to the remote
5. Open a Merge Request
6. Add "WIP" to the Merge Request title if you're not ready to have someone do a code review
7. Once you're ready and your _build is passing_, assign the Merge Request to somebody else for a code review
8. Once they have signed off, _they_ will perform the merge

### Notes

* Every Merge Request should auto-close _at least one issue_. This can be done
by mentioning the issue in one of its commit messages or the Merge Request
description, e.g, `"fixes #5"`.