# CountSwiftly

Swift implementation of the Count-Min Sketch (CMS) and the Augmented Sketch (ASketch)

A Count-Min sketch is a probabilistic data structure used for summarizing streams of data in sub-linear space.

It works as follows:
- Let `(ϵ, δ)` be two parameters that describe the confidence in our error estimates, and let `depth = ceil(ln 1/delta)` `width = ceil(e / eps)`.

Then:
- Take `depth` pairwise independent hash functions `h_i`, each of which maps onto the domain `[0, w - 1]`.
- Create a 2-dimensional table of counts, with `depth` rows and `width` columns, initialized with all zeroes.
- When a new element x arrives in the stream, update the table of counts by setting `counts[i, h_i[x]] += 1`, for each `1 <= i <= d`.

As an example application, suppose you want to estimate the number of times an element `x` has appeared in a data stream so far.
The Count-Min sketch estimate of this frequency is min_i { counts[i, h_i[x]] }.
With probability at least `1 - δ`, this estimate is within `ϵ * N` of the true frequency (i.e., `true frequency <= estimate <= true frequency + ϵ * N`), where N is the total size of the stream so far.

The Augmented Sketch, is based on a pre-filtering stage that dynamically identifies and aggregates the most frequent items.
Items overflowing the pre-filtering stage are processed using a conventional CMS algorithm, thereby making the solution general and applicable in a wide range of contexts.

## References:
   - Graham Cormode and S. Muthukrishnan. An improved data stream summary: The Count-Min sketch and its applications. 2004. https://doi.org/10.1016/j.jalgor.2003.12.001
   - Pratanu Roy, A. Khan, and G. Alonso. Augmented Sketch: Faster and More Accurate Stream Processing. 2016. https://doi.org/10.1145/2882903.2882948
   - http://dimacs.rutgers.edu/%7Egraham/pubs/papers/cm-full.pdf
   
## Examples

```swift
let seed = 7364181
srand48(seed)
let numItems = 10000
var xs = [Int](repeating: 0, count: numItems)
let maxScale: UInt32 = 20
for i in 0..<numItems {
    let scale = arc4random_uniform(maxScale)
    xs[i] = Int(arc4random_uniform(1 << scale))
}

var sketch = CountMinSketch<Int>(epsilon: 0.0001, delta: 0.99, seed: seed)
for x in xs {
    sketch.update(with: x)
}
sketch.query(for: Int(arc4random_uniform(maxScale)))

// Or how about strings
var sketch = CountMinSketch<String>(depth: 10, width: 5)
for _ in 0..<1000 {
    sketch.update(with: "a")
}
sketch.query(for: "a") // Should be 1000
sketch.query(for: "b") // Should be 0
```